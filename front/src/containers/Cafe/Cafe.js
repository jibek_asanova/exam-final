import React, {useEffect, useState} from 'react';
import {
    Card,
    CardContent,
    CardHeader,
    CardMedia,
    Grid,
    ImageList,
    ImageListItem,
    makeStyles,
    TextField,
    Typography
} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {
    createReviewRequest,
    fetchCafeRequest,
    fetchCafesPhotosRequest,
    uploadPhotoRequest
} from "../../store/actions/cafesActions";
import imageNotAvailable from "../../assets/images/not_available.png";
import {apiURL} from "../../config";
import {Rating} from "@material-ui/lab";
import FormElement from "../../components/UI/Form/FormElement";
import ButtonWithProgress from "../../components/UI/ButtonWithProgress/ButtonWithProgress";

const useStyles = makeStyles({
    card: {
        height: '100%'
    },
    media: {
        height: 0,
        paddingTop: '56.25%'
    }
})

const Cafe = ({match}) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const loading = useSelector(state => state.cafes.createReviewLoading);
    const cafe = useSelector(state => state.cafes.cafe);
    const cafeGallery = useSelector(state => state.cafes.cafeGallery);

    const [state, setState] = useState({
        rating: 0,
        comment: '',
    });
    const [gallery, setGallery] = useState({
        image: ''
    });



    useEffect(() => {
        dispatch(fetchCafeRequest(match.params.id));
    }, [dispatch, match.params.id]);

    useEffect(() => {
        dispatch(fetchCafesPhotosRequest(match.params.id));
    }, [dispatch, match.params.id]);

    const submitFormHandler = async e => {
        e.preventDefault();
        await dispatch(createReviewRequest({id: match.params.id, ...state}));
    };

    const uploadImage = async e => {
        e.preventDefault();

        const formData = new FormData();
        Object.keys(gallery).forEach(key => {
            formData.append(key, gallery[key]);
        });
        await dispatch(uploadPhotoRequest({id: match.params.id, formData}));

    }

    const handleFieldChange = event => {
        const {name, value} = event.target
        setState(prev => ({
            ...prev, [name]: value
        }))
    };

    const fileChangeHandler = e => {
        const file = e.target.files[0];
        setGallery(prevState => {
            return {...prevState, image: file};
        });

    };

    let cardImage = imageNotAvailable;

    return cafe && (
        <>
            <Grid container justifyContent="center">
                <Grid item xs={12} sm={6} md={6} lg={6}>
                    <Card className={classes.card} spacing={2}>
                        <CardHeader title={cafe.title}/>
                        {cafe.image ? <CardMedia
                            image={apiURL + '/' + cafe.image}
                            title={cafe.title}
                            className={classes.media}
                        /> : <CardMedia
                            image={cardImage}
                            title={cafe.title}
                            className={classes.media}
                        />}

                        <CardContent>
                            <Typography variant="subtitle1">
                                description: {cafe.description}
                            </Typography>
                        </CardContent>
                    </Card>
                </Grid>
                <Grid item xs={12} sm={6} md={6} lg={6}>
                    <Card className={classes.card} spacing={2}>
                        <CardContent>
                            <Typography component="h1"
                                        variant="h6">Rating: {cafe?.rating} ({cafe?.numRevies} votes)</Typography>
                            <br/>
                            {cafe.reviews.length === 0 && (
                                <Typography component="h1" variant="h6">No reviews</Typography>
                            )}
                            {
                                cafe?.reviews.map((review) => (
                                    <div key={review._id}>
                                        <Rating name="rating" value={review.rating}/>

                                        <Typography variant="subtitle1">Пользователь: {review?.name}</Typography>
                                        <Typography variant="subtitle1">Оставил
                                            комментарий: {review.comment}</Typography>
                                    </div>
                                ))
                            }
                        </CardContent>
                    </Card>
                </Grid>
            </Grid>
            <Grid item xs={12} sm={6} md={6} lg={6}
                  component="form"
                  onSubmit={submitFormHandler}

            >
                <Card className={classes.card} spacing={2}>
                    <CardContent>
                        <Rating
                            name="rating"
                            defaultValue={0}
                            size="large"
                            value={state.rating}
                            onChange={handleFieldChange}
                        />
                        <FormElement
                            multiline
                            required
                            rows={4}
                            label="Comment"
                            name="comment"
                            value={state.comment}
                            onChange={handleFieldChange}

                        />
                        <Grid item xs={12}>
                            <ButtonWithProgress
                                type="submit"
                                fullWidth
                                variant="contained"
                                color="primary"
                                className={classes.submit}
                                loading={loading}
                                disabled={loading}
                            >
                                Sumbit
                            </ButtonWithProgress>
                        </Grid>
                    </CardContent>
                </Card>
            </Grid>
            <Typography variant="h4" >Gallery</Typography>

            <ImageList sx={{ width: 500, height: 450 }} cols={3} rowHeight={164}>
                {cafeGallery.map((item) => (
                    <ImageListItem key={item.image}>
                        <img
                            src={`${apiURL + '/' + item.image}?w=164&h=164&fit=crop&auto=format`}
                            srcSet={`${apiURL + '/' + item.image}?w=164&h=164&fit=crop&auto=format&dpr=2 2x`}
                            alt={item.image}
                            loading="lazy"
                        />
                    </ImageListItem>
                ))}
            </ImageList>
            <Typography variant="h4">Add Image</Typography>
            <Grid
                container
                direction="column"
                spacing={2}
                component="form"
                className={classes.root}
                autoComplete="off"
                onSubmit={uploadImage}
                noValidate
            >
                <Grid item xs>
                    <TextField
                        type="file"
                        name="image"
                        onChange={fileChangeHandler}
                    />
                </Grid>

                <Grid item xs={12}>
                    <ButtonWithProgress
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                        loading={loading}
                        disabled={loading}
                    >
                        Upload
                    </ButtonWithProgress>
                </Grid>
            </Grid>
        </>

    );
};

export default Cafe;