import React, {useEffect} from 'react';
import {CircularProgress, Grid, Typography} from "@material-ui/core";
import CafeItem from "../../components/CafeItem/CafeItem";
import {useDispatch, useSelector} from "react-redux";
import {fetchAllCafeRequest} from "../../store/actions/cafesActions";

const AllCafe = () => {
    const dispatch = useDispatch();
    const cafes = useSelector(state => state.cafes.cafes);
    const fetchLoading = useSelector(state => state.cafes.fetchLoading);

    useEffect(() => {
        dispatch(fetchAllCafeRequest());
    }, [dispatch]);

    return (
        <Grid container direction="column" spacing={2}>
            <Grid item container justifyContent="space-between" alignItems="center">
                <Grid item>
                    <Typography variant="h4">All сafes</Typography>
                </Grid>
            </Grid>
            <Grid item>
                <Grid item container direction="row" spacing={1}>
                    {fetchLoading ? (
                        <Grid container justifyContent="center" alignItems="center">
                            <Grid item>
                                <CircularProgress/>
                            </Grid>
                        </Grid>
                    ) : cafes.map(cafe => (
                        <CafeItem
                            key={cafe._id}
                            id={cafe._id}
                            title={cafe.title}
                            image={cafe.image}
                            rating={cafe.rating}
                            reviews={cafe.numReviews}
                        />
                    ))}
                </Grid>
            </Grid>
        </Grid>
    );
};

export default AllCafe;