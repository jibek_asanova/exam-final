import {createSlice} from "@reduxjs/toolkit";

export const initialState = {
    fetchLoading: false,
    singleLoading: false,
    addCafeLoading: false,
    addCafeError: null,
    uploadPhotoLoading: false,
    uploadPhotoError: null,
    createReviewLoading: false,
    createReviewError: null,
    cafes: [],
    cafeGallery: [],
    cafe: null,
};

const name = 'cafes';

const cafesSlice = createSlice({
    name,
    initialState,
    reducers: {
        fetchAllCafeRequest(state) {
            state.fetchLoading = true;
        },
        fetchAllCafeSuccess(state, {payload: cafes}) {
            state.fetchLoading = false;
            state.cafes = cafes;
        },
        fetchAllCafeFailure(state) {
            state.fetchLoading = false;
        },
        fetchCafesPhotosRequest(state) {
            state.fetchLoading = true;
        },
        fetchCafesPhotosSuccess(state, {payload: cafeGallery}) {
            state.fetchLoading = false;
            state.cafeGallery = cafeGallery;
        },
        fetchCafesPhotosFailure(state) {
            state.fetchLoading = false;
        },
        addCafeRequest(state) {
            state.addCafeLoading = true;
        },
        addCafeSuccess(state) {
            state.addCafeLoading = false;
            state.addCafeError = null;
        },
        addCafeFailure(state, {payload: error}) {
            state.addCafeLoading = false;
            state.addCafeError = error;
        },
        fetchCafeRequest(state) {
            state.singleLoading = true;
        },
        fetchCafeSuccess(state, {payload: cafe}) {
            state.singleLoading = false;
            state.cafe = cafe;
        },
        fetchCafeFailure(state) {
            state.singleLoading = false;
        },
        createReviewRequest(state) {
            state.createReviewLoading = true;
        },
        createReviewSuccess(state, {payload: data}) {
            state.createReviewLoading = false;
            state.cafe = data;
        },
        createReviewFailure(state, action) {
            state.createReviewLoading = false;
            state.createReviewError = action.payload;
        },
        uploadPhotoRequest(state) {
            state.uploadPhotoLoading = true;
        },
        uploadPhotoSuccess(state) {
            state.uploadPhotoLoading = false;
            state.uploadPhotoError = null;
        },
        uploadPhotoFailure(state, {payload: error}) {
            state.uploadPhotoLoading = false;
            state.uploadPhotoError = error;
        },
        deleteCafeRequest(state) {
            state.fetchLoading = true;
        },
        deleteCafeSuccess(state, {payload: cafeId}) {
            state.fetchLoading = false;
            state.cafes = state.cafes.filter(cafe => cafe.id !== cafeId);
        },
        deleteCafeFailure(state) {
            state.fetchLoading = false;
        },

    }
});

export default cafesSlice;