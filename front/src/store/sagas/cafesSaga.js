import {
    addCafeFailure,
    addCafeRequest,
    addCafeSuccess,
    createReviewFailure,
    createReviewRequest,
    createReviewSuccess, deleteCafeFailure, deleteCafeRequest, deleteCafeSuccess,
    fetchAllCafeFailure,
    fetchAllCafeRequest,
    fetchAllCafeSuccess,
    fetchCafeFailure,
    fetchCafeRequest, fetchCafesPhotosFailure, fetchCafesPhotosRequest, fetchCafesPhotosSuccess,
    fetchCafeSuccess,
    uploadPhotoFailure,
    uploadPhotoRequest,
    uploadPhotoSuccess
} from "../actions/cafesActions";
import {put, takeEvery, call} from "redux-saga/effects";
import {toast} from "react-toastify";
import axiosApi from "../../axiosApi";
import {historyPush} from "../actions/historyActions";

export function* fetchCafesSagas() {
    try {
        const response = yield axiosApi.get('/cafes');
        yield put(fetchAllCafeSuccess(response.data));
    } catch (error) {
        if (error.response && error.response.data) {
            yield put(fetchAllCafeFailure(error.response.data));
            toast.error(`error: ${error.response.data.error}`);
        } else {
            yield put(fetchAllCafeFailure({global: 'No internet'}));
            toast.error('No internet');
        }
    }
}

export function* fetchCafesPhotosSagas({payload: cafeId}) {
    try {
    const response = yield axiosApi.get(`/cafes/images?cafe=${cafeId}`);
    yield put(fetchCafesPhotosSuccess(response.data));
    } catch (error) {
        if (error.response && error.response.data) {
            yield put(fetchCafesPhotosFailure(error.response.data));
            toast.error(`error: ${error.response.data.error}`);
        } else {
            yield put(fetchCafesPhotosFailure({global: 'No internet'}));
            toast.error('No internet');
        }
    }
}

export function* fetchCafeSagas({payload: id}) {
    try {
        const response = yield axiosApi.get('/cafes/' + id);
        yield put(fetchCafeSuccess(response.data))
    } catch (error) {
        if (error.response && error.response.data) {
            yield put(fetchCafeFailure(error.response.data));
            toast.error(`error: ${error.response.data.error}`);
        } else {
            yield put(fetchCafeFailure({global: 'No internet'}));
            toast.error('No internet');
        }
    }
}

export function* addCafeSagas({payload: cafeData}) {
    try {
        yield axiosApi.post('/cafes', cafeData);
        yield put(addCafeSuccess());
        historyPush('/');
        toast.success('cafe added');
    } catch (e) {
        yield put(addCafeFailure(e.response.data));
        toast.error('Could not add cafe');
    }
}

export function* createReviewSaga({payload: data}) {
    try {
        const response = yield axiosApi.post(`/cafes?cafe=${data.id}`, data);
        yield put(createReviewSuccess(response.data));
    } catch (error) {
        yield put(createReviewFailure(error.response.data));
        toast.error(error.response.data.error);
    }
}

export function* uploadPhotoSagas({payload: photoData}) {
    try {
        yield axiosApi.post(`/cafes/uploadImage?cafe=${photoData.id}`, photoData.formData);
        yield call(fetchCafesPhotosSagas, {payload: photoData.id});
        yield put(uploadPhotoSuccess());
        toast.success('photo created');
    } catch (e) {
        yield put(uploadPhotoFailure(e.response.data));
        toast.error('Could not create photo');
    }
}
export function* deleteCafe(id) {
    try {
        yield axiosApi.delete('cafes/' + id.payload);
        yield put(deleteCafeSuccess(id.payload));
        yield put(fetchAllCafeRequest());
        toast.success('Cafe deleted');
    } catch (error) {
        yield put(deleteCafeFailure(error.response.data));
        toast.error('Could not delete cafe');
    }
}




const cafesSaga = [
    takeEvery(fetchAllCafeRequest, fetchCafesSagas),
    takeEvery(addCafeRequest, addCafeSagas),
    takeEvery(fetchCafeRequest, fetchCafeSagas),
    takeEvery(createReviewRequest, createReviewSaga),
    takeEvery(uploadPhotoRequest, uploadPhotoSagas),
    takeEvery(fetchCafesPhotosRequest, fetchCafesPhotosSagas),
    takeEvery(deleteCafeRequest, deleteCafe)
];

export default cafesSaga;