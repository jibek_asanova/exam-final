import {all} from 'redux-saga/effects';
import usersSaga from "./sagas/usersSagas";
import cafesSaga from "./sagas/cafesSaga";

export function* rootSagas() {
  yield all([
      ...usersSaga,
      ...cafesSaga
  ])
}