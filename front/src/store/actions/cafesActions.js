import cafesSlices from "../slices/cafesSlices";

export const {
    fetchAllCafeRequest,
    fetchAllCafeSuccess,
    fetchAllCafeFailure,
    addCafeRequest,
    addCafeSuccess,
    addCafeFailure,
    fetchCafeFailure,
    fetchCafeRequest,
    fetchCafeSuccess,
    createReviewSuccess,
    createReviewFailure,
    createReviewRequest,
    uploadPhotoSuccess,
    uploadPhotoFailure,
    uploadPhotoRequest,
    fetchCafesPhotosFailure,
    fetchCafesPhotosRequest,
    fetchCafesPhotosSuccess,
    deleteCafeFailure,
    deleteCafeRequest,
    deleteCafeSuccess
} = cafesSlices.actions;