import {
    Button,
    Card,
    CardActions,
    CardHeader,
    CardMedia,
    Grid,
    IconButton,
    makeStyles,
    Typography,
} from "@material-ui/core";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import PropTypes from 'prop-types';
import {Link} from "react-router-dom";
import imageNotAvailable from '../../assets/images/not_available.png';
import {apiURL} from "../../config";
import {useDispatch, useSelector} from "react-redux";
import {Rating} from "@material-ui/lab";
import React from "react";
import {deleteCafeRequest} from "../../store/actions/cafesActions";

const useStyles = makeStyles({
    card: {
        height: '100%'
    },
    media: {
        height: 0,
        paddingTop: '56.25%'
    }
})

const CafeItem = ({title, id, image, rating, reviews}) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);


    let cardImage = imageNotAvailable;

    if (image) {
        cardImage = apiURL + '/' + image;
    }


    return (
        <>
            {user && user.role === 'admin' ?
                <Grid item xs={12} sm={6} md={6} lg={3} >
                    <Card className={classes.card}>
                        <CardHeader title={title}/>
                        <CardMedia
                            image={cardImage}
                            title={title}
                            className={classes.media}
                        />
                        <Rating name="rating" value={rating}/>
                        <Typography variant="subtitle1">({reviews} reviews)</Typography>
                        <CardActions>
                            <IconButton component={Link} to={'/cafes/' + id}>
                                <ArrowForwardIcon/>
                            </IconButton>
                            <Button onClick={() => dispatch(deleteCafeRequest(id))}>Delete</Button>
                        </CardActions>
                    </Card>
                </Grid> :
                <Grid item xs={12} sm={6} md={6} lg={3}>
                    <Card className={classes.card}>
                        <CardHeader title={title}/>
                        <CardMedia
                            image={cardImage}
                            title={title}
                            className={classes.media}
                        />
                        <Rating name="rating" value={rating}/>
                        <Typography variant="subtitle1">({reviews} reviews)</Typography>
                        <CardActions>
                            <IconButton component={Link} to={'/cafes/' + id}>
                                <ArrowForwardIcon/>
                            </IconButton>
                        </CardActions>
                    </Card>
                </Grid>
            }
        </>

    );
};

CafeItem.propTypes = {
    title: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
    image: PropTypes.string
}

export default CafeItem;