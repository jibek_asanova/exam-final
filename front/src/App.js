import Layout from "./components/UI/Layout/Layout";
import {Route, Switch} from "react-router-dom";
import Login from "./containers/Login/Login";
import Register from "./containers/Register/Register";
import AllCafe from "./containers/AllCafe/AllCafe";
import AddCafe from "./containers/AddCafe/AddCafe";
import Cafe from "./containers/Cafe/Cafe";

const App = () => {
    return (
        <Layout>
            <Switch>
                <Route path="/" exact component={AllCafe}/>
                <Route path="/add" component={AddCafe}/>
                <Route path="/add" component={AddCafe}/>
                <Route path="/cafes/:id" component={Cafe}/>
                <Route path="/login" component={Login}/>
                <Route path="/register" component={Register}/>
            </Switch>
        </Layout>
    );
};

export default App;
