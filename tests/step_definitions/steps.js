const { I } = inject();
// Add in your custom step files

Given('я захожу на страницу {string}', (page) => {
  I.amOnPage('/' + page);
});

Given('я ввожу данные:', (table) => {
  table.rows.forEach(row => {
    const name = row.cells[0].value;
    const value = row.cells[1].value;
    I.fillField(name, value);
  });
});

When('нажимаю на кнопку {string}', (buttonText) => {
  if(buttonText === 'Sign up') {
    I.click('//form//button[.//span[contains(text(), \'Sign up\')]]');
  } else {
    I.click(buttonText);
  }
  I.wait(10);
});

Then('я вижу текст {string}', (text) => {
  I.see(text);
});
