const mongoose = require('mongoose');
const {nanoid} = require('nanoid');
const config = require('./config');
const User = require("./models/User");
const Cafe = require("./models/Cafe");
const Photo = require("./models/Photo");

const run = async () => {
  await mongoose.connect(config.db.url);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }
  const [admin, user1, user2, user3] = await User.create({
      email: 'admin@gmail.com',
      password: 'admin',
      token: nanoid(),
      role: 'admin',
      displayName: 'Admin',
      avatarImage: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR1obN7zovv6kbr6dDQcf26ZJHFbKOCDXjMxVZh0LLrSenzaxL2'
    }, {
      email: 'jibek@gmail.com',
      password: '123',
      token: nanoid(),
      role: 'user',
      displayName: 'Jibek',
      avatarImage: 'https://upload.wikimedia.org/wikipedia/commons/4/48/Rapper_B.o.B_2013.jpg'
    },
    {
      email: 'john@gmail.com',
      password: '123',
      token: nanoid(),
      role: 'user',
      displayName: 'John',
      avatarImage: 'https://upload.wikimedia.org/wikipedia/commons/4/48/Rapper_B.o.B_2013.jpg'
    },
    {
      email: 'dan@gmail.com',
      password: '123',
      token: nanoid(),
      role: 'user',
      displayName: 'Dan',
      avatarImage: 'https://upload.wikimedia.org/wikipedia/commons/4/48/Rapper_B.o.B_2013.jpg'
    },
  )

  const [cafe1, cafe2, cafe3] = await Cafe.create({
      user: user1,
      title: 'CAFE De PARIS',
      description: 'Franch cafe',
      image: 'fixtures/cafeDeParis.png',
      reviews: [
        {
          user: user1,
          name: user1.displayName,
          rating: 5,
          comment: 'Amazing food'
        }, {
          admin,
          name: admin.displayName,
          rating: 4,
          comment: 'Good food'
        }
      ],
      rating: 4.5,
    numReviews: 2

    },
    {
      user: user2,
      title: 'Dodo Pizza',
      description: 'pizza cafe',
      image: 'fixtures/dodo.jpeg',
      reviews: [
        {
          user: user2,
          name: user2.displayName,
          rating: 5,
          comment: 'Amazing food'
        }, {
          user: user2,
          name: user2.displayName,
          rating: 4,
          comment: 'Good food'
        }
      ],
      rating: 4.5,
      numReviews: 2
    },
    {
      user: admin,
      title: 'Navat',
      description: 'kyrgyz national food',
      image: 'fixtures/navat.jpg',
      reviews: [
        {
          user: admin,
          name: user2.displayName,
          rating: 5,
          comment: 'Amazing food'
        }, {
          user: user2,
          name: user2.displayName,
          rating: 4,
          comment: 'Good food'
        }
      ],
      rating: 4.5,
      numReviews: 2
    })

  await Photo.create({
      image: 'fixtures/cafeDeParis.png',
      userId: user1,
      cafeId: cafe1,
    },
    {
      image: 'fixtures/dodo.jpeg',
      userId: user1,
      cafeId: cafe1,
    },
    {
      image: 'fixtures/navat.jpg',
      userId: user1,
      cafeId: cafe1,
    })
  await mongoose.connection.close();

};

run().catch(console.error)