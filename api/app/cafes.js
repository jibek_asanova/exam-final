const express = require('express');
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid');
const config = require('../config');
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");
const Cafe = require("../models/Cafe");
const Photo = require("../models/Photo");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', async (req, res) => {
  try {
    const allCafe = await Cafe.find();
    res.send(allCafe);
  } catch (e) {
    res.status(500).send({error: 'Internal Server Error'});
  }
});

router.get('/images', async (req, res) => {
  try {
    if(req.query.cafe) {
      const photos = await Photo.find({cafeId: req.query.cafe});
      res.send(photos);
    }
  } catch (e) {
    res.status(500).send({error: 'Internal Server Error'});
  }
})

router.get('/:id', async (req, res) => {
  try {
    const cafe = await Cafe.findById(req.params.id).populate('user', 'displayName');
    if (cafe) {
      res.send(cafe);
    } else {
      res.status(404).send({error: 'Cafe not found'});
    }
  } catch {
    res.sendStatus(500);
  }
});

router.post('/', auth, permit('user', 'admin'), upload.single('image'), async (req, res) => {
  try {
    if(req.query.cafe) {
      const {rating, comment} = req.body;
      const cafe = await Cafe.findById(req.query.cafe).populate('user', 'displayName');

      if (cafe) {
        const reviewData = {
          user: req.user._id,
          name: req.user.displayName,
          rating: Number(rating),
          gallery: [],
          comment
        }

        if(req.files){
          req.files.forEach(f => reviewData.gallery.push('uploads/' +  f.filename));
        }

        cafe.reviews.push(reviewData);
        cafe.numReviews = cafe.reviews.length;
        cafe.rating =
          cafe.reviews.reduce((acc, item) => item.rating + acc, 0) /
          cafe.reviews.length;
        await cafe.save();
        res.send(cafe);
      }
    }
    const cafeData = {
      title: req.body.title,
      description: req.body.description,
      user: req.user._id,
    };


    if (req.file) {
      cafeData.image = 'uploads/' + req.file.filename
    }

    const cafe = new Cafe(cafeData);
    await cafe.save();
    res.send(cafe);

  } catch (error) {
    res.status(400).send(error);
  }
});

router.post('/uploadImage', auth, permit('user', 'admin'), upload.single('image'), async (req, res) => {
  try {
    if(req.query.cafe) {
        const imageData = {
          userId: req.user._id,
          cafeId: req.query.cafe
        }

        if (req.file) {
          imageData.image = 'uploads/' + req.file.filename
        }

      const photo = new Photo(imageData);
      await photo.save();
      res.send(photo);
    }
  } catch (error) {
    res.status(400).send(error);
  }
})

router.delete('/:id', auth, permit('admin'), async (req, res) => {
  try {
    const cafe = await Cafe.findByIdAndDelete(req.params.id);

    if (cafe) {
      res.send(`cafe ${cafe.title} removed`);
    } else {
      res.status(404).send({error: 'Cocktail not found'})
    }
  } catch (e) {
    res.sendStatus(500);
  }
});


module.exports = router;