const mongoose = require('mongoose');

const PhotoSchema = new mongoose.Schema({
  image: {
    type: String,
    required: true,
  },
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },
  cafeId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Cafe',
    required: true
  }
});

const Photo = mongoose.model('Photo', PhotoSchema);

module.exports = Photo;